package com.esrlabs.rssreader.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import com.esrlabs.rssreader.R;
import com.esrlabs.rssreader.activity.FeedListActivity;
import com.esrlabs.rssreader.model.FeedItem;
import com.esrlabs.rssreader.parser.FeedParser;
import com.esrlabs.rssreader.parser.FeedParserException;
import com.esrlabs.rssreader.parser.SAXFeedParser;
import com.esrlabs.rssreader.utils.Closer;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class FeedUpdateService extends IntentService {

    private static final String SERVICE_NAME = "FEED_UPDATE_SERVICE";
    private static final String NEWS_RSS_FEED_URL = "http://feeds.bbci.co.uk/news/rss.xml";
    private FeedParser feedParser = new SAXFeedParser();

    public FeedUpdateService() {
        super(SERVICE_NAME);
    }

    public FeedUpdateService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Intent dataLoadedIntent = new Intent(FeedListActivity.ACTION_FEED_LOADING_COMPLETED);
        try {
            List<FeedItem> feedItems = fetchFeedItems();
            dataLoadedIntent.putParcelableArrayListExtra(FeedListActivity.FEED_ITEMS_KEY, new ArrayList<FeedItem>(feedItems));
        } catch (IOException e) {
            dataLoadedIntent.putExtra(FeedListActivity.FEED_ERROR_MESSAGE_KEY, getString(R.string.feed_loading_connection_error));
        } catch (FeedParserException e) {
            dataLoadedIntent.putExtra(FeedListActivity.FEED_ERROR_MESSAGE_KEY, getString(R.string.feed_loading_data_parsing_error));
        }

        sendBroadcast(dataLoadedIntent);
    }

    private List<FeedItem> fetchFeedItems() throws IOException, FeedParserException{
        URL url = new URL(NEWS_RSS_FEED_URL);
        URLConnection connection = url.openConnection();
        InputStream feedStream = connection.getInputStream();
        final List<FeedItem> feedItems = feedParser.parse(feedStream);
        Closer.closeSilently(feedStream);
        return feedItems;
    }
}
