package com.esrlabs.rssreader;

import android.app.Application;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.esrlabs.rssreader.utils.BitmapLruCache;

public class RSSReaderApplication extends Application {

    private RequestQueue queue;
    private ImageLoader.ImageCache imageCache;

    @Override
    public void onCreate() {
        super.onCreate();

        queue = Volley.newRequestQueue(this);
        imageCache = new BitmapLruCache();
    }

    public RequestQueue getRequestQueue() {
        return queue;
    }

    public ImageLoader.ImageCache getImageCache() {
        return imageCache;
    }
}
