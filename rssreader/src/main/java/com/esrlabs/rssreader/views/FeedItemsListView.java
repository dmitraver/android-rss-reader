package com.esrlabs.rssreader.views;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.esrlabs.rssreader.R;
import com.esrlabs.rssreader.RSSReaderApplication;
import com.esrlabs.rssreader.activity.FeedItemActivity;
import com.esrlabs.rssreader.model.FeedItem;

import java.util.List;

public class FeedItemsListView extends ListView {
    private FeedItemsAdapter adapter;

    public FeedItemsListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public FeedItemsListView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FeedItemsListView(Context context) {
        this(context, null, 0);
    }

    public void setFeedItems(List<FeedItem> feedItems) {
        adapter.setFeedItems(feedItems);
    }

    private void init(Context context) {
        adapter = new FeedItemsAdapter(context);
        setAdapter(adapter);
    }

    private class FeedItemsAdapter extends BaseAdapter {

        private LayoutInflater inflater;
        private List<FeedItem> feedItems;
        private ImageLoader imageLoader;

        public FeedItemsAdapter(Context context) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            RSSReaderApplication application = (RSSReaderApplication) context.getApplicationContext();
            imageLoader = new ImageLoader(application.getRequestQueue(), application.getImageCache());
        }

        public void setFeedItems(List<FeedItem> feedItems) {
            this.feedItems = feedItems;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return feedItems == null ? 0 : feedItems.size();
        }

        @Override
        public FeedItem getItem(int position) {
            return feedItems.get(position);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        private class ViewHolder {
            TextView title;
            TextView description;
            NetworkImageView image;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final ViewHolder viewHolder;

            if (convertView == null || convertView.getTag() == null) {
                convertView = inflater.inflate(R.layout.feed_list_item, viewGroup, false);

                viewHolder = new ViewHolder();
                viewHolder.title = (TextView) convertView.findViewById(R.id.feed_list_item_title_text_view);
                viewHolder.description = (TextView) convertView.findViewById(R.id.feed_list_item_description_text_view);
                viewHolder.image = (NetworkImageView) convertView.findViewById(R.id.feed_list_item_image_view);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final FeedItem feedItem = getItem(position);
            viewHolder.title.setText(feedItem.getTitle());
            viewHolder.description.setText(feedItem.getDescription());
            viewHolder.image.setErrorImageResId(R.drawable.no_image);
            if(feedItem.getLink() != null) {
                viewHolder.image.setImageUrl(feedItem.getImageLink(), imageLoader);
            }

            convertView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), FeedItemActivity.class);
                    intent.putExtra(FeedItemActivity.TITLE_EXTRA_KEY, feedItem.getTitle());
                    intent.putExtra(FeedItemActivity.DESCRIPTION_EXTRA_KEY, feedItem.getDescription());
                    intent.putExtra(FeedItemActivity.FEED_ITEM_LINK_EXTRA_KEY, feedItem.getLink());
                    getContext().startActivity(intent);
                }
            });

            return convertView;
        }
    }
}
