package com.esrlabs.rssreader.model;

import android.os.Parcel;
import android.os.Parcelable;

public class FeedItem  implements Parcelable {
    private String title;
    private String description;
    private String link;
    private String imageLink;
    private String publicationDate;

    public void setTitle(String title) {
        this.title = title.trim();
    }

    public void setDescription(String description) {
        this.description = description.trim();
    }

    public void setLink(String link) {
        this.link = link.trim();
    }

    public void setImageLink(String link) {
        this.imageLink = link.trim();
    }

    public void setPublicationDate(String pubDate) {
        this.publicationDate = pubDate.trim();
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLink() {
        return link;
    }

    public String getImageLink() {
        return imageLink;
    }

    public String getPublicationDate() {
        return publicationDate;
    }

    public static final Creator<FeedItem> CREATOR = new Creator<FeedItem>() {
        @Override
        public FeedItem createFromParcel(Parcel parcel) {
            FeedItem item = new FeedItem();
            item.title = parcel.readString();
            item.description = parcel.readString();
            item.link = parcel.readString();
            item.imageLink = parcel.readString();
            item.publicationDate = parcel.readString();
            return item;
        }

        @Override
        public FeedItem[] newArray(int size) {
            return new FeedItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(link);
        parcel.writeString(imageLink);
        parcel.writeString(publicationDate);
    }
}
