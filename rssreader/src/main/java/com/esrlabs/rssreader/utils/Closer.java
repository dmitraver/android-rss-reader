package com.esrlabs.rssreader.utils;

import java.io.Closeable;
import java.io.IOException;

public final class Closer {

    private Closer() {
    }

    public static void closeSilently(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
