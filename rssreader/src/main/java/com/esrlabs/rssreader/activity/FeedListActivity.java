package com.esrlabs.rssreader.activity;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.Toast;
import com.esrlabs.rssreader.R;
import com.esrlabs.rssreader.model.FeedItem;
import com.esrlabs.rssreader.service.FeedUpdateService;
import com.esrlabs.rssreader.views.FeedItemsListView;

import java.util.ArrayList;

public class FeedListActivity extends Activity {

    public static final String FEED_ERROR_MESSAGE_KEY = "feed_update_error";
    public static final String FEED_ITEMS_KEY = "feed_items";
    public static final String ACTION_FEED_LOADING_COMPLETED = "feed_loading_completed";

    private static final long CHECK_FOR_UPDATE_INTERVAL_MILLIS = 60000;

    private FeedItemsListView list;
    private PendingIntent feedUpdateServiceIntent;
    private AlarmManager alarmManager;

    private BroadcastReceiver feedUpdateReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            if(extras != null) {
                String errorMessage = extras.getString(FEED_ERROR_MESSAGE_KEY);
                if(errorMessage != null) {
                    Toast.makeText(FeedListActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                } else {
                    ArrayList<FeedItem> feedItems = extras.getParcelableArrayList(FEED_ITEMS_KEY);
                    list.setFeedItems(feedItems);
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_list);

        list = (FeedItemsListView) findViewById(R.id.activity_feed_list_feed_list_view);
    }

    private void scheduleCyclicFeedUpdateTask() {
        Intent intent = new Intent(this, FeedUpdateService.class);
        feedUpdateServiceIntent = PendingIntent.getService(this, 0, intent, 0);

        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), CHECK_FOR_UPDATE_INTERVAL_MILLIS, feedUpdateServiceIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(feedUpdateReceiver, new IntentFilter(ACTION_FEED_LOADING_COMPLETED));
        scheduleCyclicFeedUpdateTask();
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(feedUpdateReceiver);
        stopCyclicFeedUpdateTask();
    }

    private void stopCyclicFeedUpdateTask() {
        alarmManager.cancel(feedUpdateServiceIntent);
    }
}
