package com.esrlabs.rssreader.activity;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.esrlabs.rssreader.R;

public class FeedItemActivity extends Activity {

    public static final String TITLE_EXTRA_KEY = "title";
    public static final String DESCRIPTION_EXTRA_KEY = "description";
    public static final String FEED_ITEM_LINK_EXTRA_KEY = "feed_item_link";

    private TextView feedItemTitle;
    private TextView feedItemDescription;
    private Button showFeedItemInBrowserButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_item);

        feedItemTitle = (TextView) findViewById(R.id.activity_feed_item_title_text_view);
        feedItemDescription = (TextView) findViewById(R.id.activity_feed_item_description_text_view);
        showFeedItemInBrowserButton = (Button) findViewById(R.id.activity_feed_item_show_in_browser_button);

        final Bundle bundle = getIntent().getExtras();
        String title = bundle.getString(TITLE_EXTRA_KEY);
        feedItemTitle.setText(title);

        String description = bundle.getString(DESCRIPTION_EXTRA_KEY);
        feedItemDescription.setText(description);

        showFeedItemInBrowserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String feedItemLink = bundle.getString(FEED_ITEM_LINK_EXTRA_KEY);
                Intent viewIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(feedItemLink));
                startActivity(viewIntent);
            }
        });
    }
}
