package com.esrlabs.rssreader.parser;

import com.esrlabs.rssreader.model.FeedItem;

import java.io.InputStream;
import java.util.List;

public interface FeedParser {
    List<FeedItem> parse(InputStream stream) throws FeedParserException;
}
