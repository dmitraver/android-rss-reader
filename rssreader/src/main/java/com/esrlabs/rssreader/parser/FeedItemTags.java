package com.esrlabs.rssreader.parser;

final class FeedItemTags {
    private FeedItemTags() {
    }

    public static final String ITEM = "item";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String LINK = "link";
    public static final String IMAGE = "media:thumbnail";
    public static final String IMAGE_URL_ATTRIBUTE = "url";
    public static final String PUBLICATION_DATE = "pubDate";
}
