package com.esrlabs.rssreader.parser;

public class FeedParserException extends Exception {
    public FeedParserException() {
        super();
    }

    public FeedParserException(String detailMessage) {
        super(detailMessage);
    }

    public FeedParserException(Throwable throwable) {
        super(throwable);
    }

    public FeedParserException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }
}
