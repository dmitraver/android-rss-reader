package com.esrlabs.rssreader.parser;

import com.esrlabs.rssreader.model.FeedItem;
import com.esrlabs.rssreader.utils.Closer;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;
import java.util.List;

public class SAXFeedParser implements FeedParser {

    private SAXParser parser;
    private FeedItemTagHandler tagHandler;

    public SAXFeedParser() {
        try {
            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            parser = parserFactory.newSAXParser();
            tagHandler = new FeedItemTagHandler();
        } catch (Exception e) {
            throw new RuntimeException("SAXFeedParser initialization error");
        }
    }

    @Override
    public List<FeedItem> parse(InputStream stream) throws FeedParserException {
        List<FeedItem> items = null;
        try {
            parser.parse(stream, tagHandler);
        } catch (Exception e) {
            throw new FeedParserException("Can't parse rss feed from stream", e);
        } finally {
            Closer.closeSilently(stream);
        }

        return tagHandler.getFeedItems();
    }
}
