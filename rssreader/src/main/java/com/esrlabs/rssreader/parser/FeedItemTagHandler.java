package com.esrlabs.rssreader.parser;

import com.esrlabs.rssreader.model.FeedItem;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class FeedItemTagHandler extends DefaultHandler {

    private List<FeedItem> items = new ArrayList<FeedItem>();
    private FeedItem currentItem;
    private StringBuilder buffer = new StringBuilder();

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);

        if (localName.equalsIgnoreCase(FeedItemTags.ITEM)) {
            currentItem = new FeedItem();
        } else if (qName.equalsIgnoreCase(FeedItemTags.IMAGE)) {
            if (currentItem != null && (currentItem.getImageLink() == null)) {
                for (int i = 0; i < attributes.getLength(); i++) {
                    String attrName = attributes.getLocalName(i);
                    if (attrName.equalsIgnoreCase(FeedItemTags.IMAGE_URL_ATTRIBUTE)) {
                        currentItem.setImageLink(attributes.getValue(i));
                    }
                }
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        if (this.currentItem != null) {
            if (localName.equalsIgnoreCase(FeedItemTags.TITLE)) {
                currentItem.setTitle(buffer.toString());
            } else if (localName.equalsIgnoreCase(FeedItemTags.LINK)) {
                currentItem.setLink(buffer.toString());
            } else if (localName.equalsIgnoreCase(FeedItemTags.DESCRIPTION)) {
                currentItem.setDescription(buffer.toString());
            } else if (localName.equalsIgnoreCase(FeedItemTags.PUBLICATION_DATE)) {
                currentItem.setPublicationDate(buffer.toString());
            } else if (localName.equalsIgnoreCase(FeedItemTags.ITEM)) {
                items.add(currentItem);
            }

            buffer.setLength(0);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
        buffer.append(ch, start, length);
    }

    public List<FeedItem> getFeedItems() {
        return Collections.unmodifiableList(items);
    }

}
